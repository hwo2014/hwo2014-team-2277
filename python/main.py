import json
import socket
import sys
import logging
import logging.config
from race import Race
from datetime import datetime

class CuddleBot(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key
		self.race = Race()

	def msg(self, msg_type, data):
		log.debug("send[{0}]: {1}".format(msg_type, data))
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.sendall(msg + "\n")

	def join(self):
		log.debug("joining {0}: {1}".format(self.key, self.name))
		return self.msg("join", {"name": self.name,
								 "key": self.key})

	def create(self, track):
		log.debug("creating {0}: {1} on {2}".format(self.key, self.name, track))
		return self.msg("createRace", {"botId": {
			                             "name": self.name,
			                             "key": self.key
			                           },
			                           "trackName": track,
			                           "carCount": 1})

	def throttle(self, throttle):
		self.msg("throttle", throttle)
		self.race.logThrottle(throttle)

	def ping(self):
		self.msg("ping", {})

	def run(self):
		self.join()
		#self.create('usa')
		self.msg_loop()

	def on_join(self, data):
		log.info("joined race")
		log.debug("joined: {0}".format(data))
		self.ping()

	def on_game_init(self, data):
		log.info("Race initialised")
		self.race.gameInit(json.dumps(data))
		self.ping()

	def on_game_start(self, data):
		log.info("Race started")
		throttle = self.race.getThrottle()
		self.throttle(throttle)

	def on_car_positions(self, data):
		switch = self.race.getSwitch()
		turbo = self.race.useTurbo()

		if switch != 'n':
			self.msg("switchLane", switch)
		elif turbo:
			msg = "Warp speed Mr. Sulu!"
			self.msg("turbo", msg)
			self.race.unloadTurbo()
			log.info("Turbo engaged: {0}".format(msg))
		else:
			throttle = self.race.getThrottle()
			self.throttle(throttle)

	def on_crash(self, data):
		log.info("Someone crashed")
		self.ping()

	def on_game_end(self, data):
		log.info("Race ended")
		self.ping()
	
	def on_turbo_available(self, data):
		log.info("Turbo acquired!")
		self.race.loadTurbo(data)
		self.ping()

	def on_turbo_start(self, data):
		self.race.turboOn(data)
		self.ping()

	def on_turbo_end(self, data):
		self.race.turboOff(data)
		self.ping()

	def on_error(self, data):
		log.error("Error: {0}".format(data))
		self.ping()

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'gameStart': self.on_game_start,
			'gameInit': self.on_game_init,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'turboAvailable': self.on_turbo_available,
			'turboStart': self.on_turbo_start,
			'turboEnd': self.on_turbo_end,
			'error': self.on_error,
		}
		socket_file = s.makefile()

		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			log.debug(json.dumps(msg, sort_keys=True, indent=2,
			                     separators=(',', ': ')))

			msg_type, data = msg['msgType'], msg['data']

			if msg_type == 'carPositions':
				self.race.logCarPositions(line)

			if msg_type in msg_map:
				msg_map[msg_type](data)
			else:
				log.info("Got {0}".format(msg_type))
				self.ping()

			line = socket_file.readline()
		
		if log.getEffectiveLevel() == logging.DEBUG:
		    filename = '../data/racelog-' + datetime.now().strftime("%Y%m%d%H%M%S") + '.csv'
		    self.race.saveCarPositionLog(filename)


if __name__ == "__main__":
	logging.config.fileConfig('logging.conf')
	log = logging.getLogger("main")
	log.info("Starting application")

	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		log.info("Connecting with parameters:")
		log.info(("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5])))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = CuddleBot(s, name, key)
		bot.run()
