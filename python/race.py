import json
import socket
import sys
import logging
import logging.config
import math
import csv

class Race(object):

	def __init__(self):
		logging.config.fileConfig('logging.conf')
		self.log = logging.getLogger("race")
		self.lanes = []
		self.carPositionLog = []
		self.lastThrottle = 0.0
		self.currentThrottle = 1.0
		self.turbo = {}
		self.turbo['available'] = False
		self.turbo['on'] = False
		self.turboPiece = 0
		self.switchDir = []

	def gameInit(self, data):
		gameData = json.loads(data)
		self.log.debug(json.dumps(gameData, sort_keys=True, indent=2, separators=(',', ': ')))

		for lane in gameData['race']['track']['lanes']:
			idx = lane['index']
			dfc = lane['distanceFromCenter']
			pieces = []

			for piece in gameData['race']['track']['pieces']:
				p = {}

				if piece.has_key('length'):
					p['length'] = piece['length']
					p['curve'] = 'n'

				if piece.has_key('switch'):
					p['switch'] = True
				else:
					p['switch'] = False

				if piece.has_key('radius') and piece.has_key('angle'):
					p['angle'] = piece['angle']
					if p['angle'] < 0:
						p['radius'] = piece['radius'] + dfc
						p['curve'] = 'l'
					else:
						p['radius'] = piece['radius'] - dfc
						p['curve'] = 'r'

					p['length'] = round((2.0 * math.pi * p['radius']) * (math.fabs(p['angle'])/360), 1)

				pieces.append(p)

			bigStraight = 0
			for x in range(0, len(pieces)):
				y = x + 1
				while pieces[y % len(pieces)]['curve'] == 'n':
					y += 1
				if (y > bigStraight):
					bigStraight = y
					self.turboPiece = x
			self.log.debug("turboPiece: {0}".format(self.turboPiece))

			self.lanes.insert(idx, dict([('pieces', pieces)]))

	def logCarPositions(self, data):
		cpData = json.loads(data)
		cpLogData = {}

		cpLogData['gameTick'] = cpData['gameTick'] if cpData.has_key('gameTick') else 0

		for cp in cpData['data']:
			if cp['id']['name'] != 'Factor':
				continue

			cpLogData['lap'] = cp['piecePosition']['lap']
			cpLogData['pieceIndex'] = cp['piecePosition']['pieceIndex']
			cpLogData['inPieceDistance'] = round(cp['piecePosition']['inPieceDistance'], 2)
			cpLogData['laneStart'] = cp['piecePosition']['lane']['startLaneIndex']
			cpLogData['laneEnd'] = cp['piecePosition']['lane']['endLaneIndex']
			cpLogData['carAngle'] = round(math.fabs(cp['angle']), 2)

			pieceInfo = self.getPieces(cpLogData['laneEnd'])[cpLogData['pieceIndex']]
			cpLogData['length'] = pieceInfo['length']
			cpLogData['angle'] = pieceInfo['angle'] if pieceInfo.has_key('angle') else ''
			cpLogData['radius'] = pieceInfo['radius'] if pieceInfo.has_key('radius') else ''
			cpLogData['curve'] = pieceInfo['curve']
			cpLogData['switch'] = pieceInfo['switch']
			cpLogData['throttle'] = self.lastThrottle

			cpLogData['velocity'] = 0
			cpLogData['angleDelta'] = 0
			cpLogData['pieceChange'] = False
			if len(self.carPositionLog) > 0:
				prevLog = self.carPositionLog[-1]
				if prevLog['pieceIndex'] != cpLogData['pieceIndex']:
					cpLogData['velocity'] = prevLog['length'] - prevLog['inPieceDistance'] + cpLogData['inPieceDistance']
					cpLogData['pieceChange'] = True
				else:
					cpLogData['velocity'] = cpLogData['inPieceDistance'] - prevLog['inPieceDistance']

				cpLogData['velocity'] = round(cpLogData['velocity'], 2)
				cpLogData['angleDelta'] = cpLogData['carAngle'] - prevLog['carAngle']

			self.carPositionLog.append(cpLogData)
			self.log.debug("pce: {0}, vel: {1}, cur: {2}, thr: {3}, ang: {4}, del: {5}".format(
				cpLogData['pieceIndex'],cpLogData['velocity'],cpLogData['curve'],
				cpLogData['throttle'],cpLogData['carAngle'],cpLogData['angleDelta']))

	def saveCarPositionLog(self, filename):
		fields = ["gameTick", "lap", "pieceIndex", "inPieceDistance",
				  "laneStart", "laneEnd", "pieceChange", "switch",
				  "carAngle", "angleDelta", "length", "curve", "angle",
				  "radius", "throttle", "velocity"]

		f = open(filename, "wb")
		cpl = csv.DictWriter(f, fieldnames=fields,dialect='excel')
		cpl.writeheader()
		cpl.writerows(self.carPositionLog)
		f.close()

	def logThrottle(self, throttle):
		self.lastThrottle = throttle

	def loadTurbo(self, data):
		self.turbo['available'] = True
		self.turbo['turboDurationMilliseconds'] = 500.0
		self.turbo['turboDurationTicks'] = 30
		self.turbo['turboFactor'] = 3.0

	def useTurbo(self):
		if not self.turbo['available']:
			return False

		cpLog = self.carPositionLog[-1]
		if cpLog['lap'] == 2 and cpLog['pieceIndex'] == self.turboPiece:
			return True
		
		return False
	
	def unloadTurbo(self):
		self.turbo['available'] = False

	def turboOn(self, data):
		if data['name'] == "Factor":
			self.turbo['on'] = True

	def turboOff(self, data):
		if data['name'] == "Factor":
			self.turbo['on'] = False

	def getPieces(self, laneIndex):
		return self.lanes[laneIndex]['pieces']

	def nextCurve(self, pieceIndex):
		pieces = self.getPieces(0)
		c = len(pieces)
		x = pieceIndex + 2
		while c > 0:
			if pieces[x]['curve'] != 'n':
				return 'Left' if pieces[x]['curve'] == 'l' else 'Right'
			x = (x + 1) % len(pieces)
			c -= 1

	def piecesToNextCurve(self, pieceIndex):
		pieces = self.getPieces(0)
		x = (pieceIndex + 1) % len(pieces)
		pCount = 0
		while pieces[x]['curve'] == 'n':
			pCount += 1
			x = (x + 1) % len(pieces)
		
		return pCount

	def getSwitch(self):
		cpLog = self.carPositionLog[-1]
		pieces = self.getPieces(cpLog['laneEnd'])
		nextPiece = pieces[(cpLog['pieceIndex'] + 1) % len(pieces)]
		if nextPiece['switch'] and (cpLog['pieceChange'] or cpLog['gameTick'] == 4):
			return self.nextCurve(cpLog['pieceIndex'])
		else:
			return 'n'

	def getThrottle(self):
		if len(self.carPositionLog) <= 0:
			return self.currentThrottle

		# get car data
		cpLog = self.carPositionLog[-1]
		pieces = self.getPieces(cpLog['laneEnd'])

		stretch = []
		for x in range(0, 4):
			stretch.append(pieces[(cpLog['pieceIndex'] + x) % len(pieces)])

		self.currentThrottle = 1.0

		# curve handling
		if stretch[0]['curve'] != 'n':
			if math.fabs(cpLog['carAngle']) + round(cpLog['angleDelta'] + 0.5, 0) * 12 > 50.0:
				self.currentThrottle = 0.0
			else:
				self.currentThrottle = 1.0

		# straight handling
		if stretch[0]['curve'] == 'n':
			if cpLog['velocity'] - 6.5 > 1.2 * self.piecesToNextCurve(cpLog['pieceIndex']):
				self.currentThrottle = 0.0
			elif stretch[0]['curve'] == 'n' and stretch[1]['curve'] == 'n' and stretch[2]['curve'] == 'n':
				self.currentThrottle = 1.0
			elif stretch[0]['curve'] == 'n' and stretch[1]['curve'] == 'n' and stretch[2]['curve'] != 'n' and cpLog['velocity'] > 8.5:
				self.currentThrottle = 0.0
			elif stretch[0]['curve'] == 'n' and stretch[1]['curve'] != 'n' and cpLog['velocity'] > 7.0:
				self.currentThrottle = 0.0
			elif cpLog['velocity'] < 6.6:
				self.currentThrottle = 1.0
			else:
				self.currentThrottle = 0.66

		# make sure we are always within the range
		if self.currentThrottle > 1.0:
			self.currentThrottle = 1.0
		if self.currentThrottle < 0.0:
			self.currentThrottle = 0.0

		return self.currentThrottle
